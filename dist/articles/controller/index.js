"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getArticles = exports.addArticles = void 0;
const use_case_1 = require("../use-case");
const add_articles_1 = __importDefault(require("./add-articles"));
const get_articles_1 = __importDefault(require("./get-articles"));
const addArticles = (0, add_articles_1.default)({ createDataArticles: use_case_1.createDataArticles });
exports.addArticles = addArticles;
const getArticles = (0, get_articles_1.default)({ getDataArticles: use_case_1.getDataArticles });
exports.getArticles = getArticles;
const kumparanController = Object.freeze({
    addArticles,
    getArticles
});
exports.default = kumparanController;
