"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function makeArticlesDb({ Query }) {
    return Object.freeze({
        createDataArticles,
        showDataArticles
    });
    function createDataArticles(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let sql = `INSERT INTO articles (author, title, body, created)
                      VALUES ('${body.getAuthor()}', '${body.getTitle()}', '${body.getBody()}', '${body.getCreatedTime()}');`;
                        let result = yield Query(sql);
                        resolve(result.rowCount);
                    }
                    catch (error) {
                        reject(new Error('articlesDb-createDataArticles ' + error));
                    }
                });
            });
        });
    }
    function showDataArticles(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let where = 'WHERE 1=1 ';
                        if (body.query) {
                            where += " AND (title LIKE '%" + body.query + "%' OR body LIKE'%" + body.query + "%') ";
                        }
                        if (body.author) {
                            where += " AND author = '" + body.author + "' ";
                        }
                        const sql = `SELECT * FROM articles ${where} ORDER BY created desc`;
                        console.log('sql', sql);
                        let result = yield Query(sql);
                        resolve(result.rows);
                    }
                    catch (error) {
                        reject(new Error('articlesDb-showDataArticles ' + error));
                    }
                });
            });
        });
    }
}
exports.default = makeArticlesDb;
