"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const camelcase_keys_1 = __importDefault(require("camelcase-keys"));
const helpers_1 = require("./helpers");
const app = (0, express_1.default)();
const makeCallback = require('./call-back');
const controller_1 = require("./controller");
app.use((0, cors_1.default)());
app.use(body_parser_1.default.json());
app.post('/articles', makeCallback(controller_1.addArticles, camelcase_keys_1.default));
app.get('/articles', makeCallback(controller_1.getArticles, camelcase_keys_1.default));
app.use((err, req, res, next) => {
    (0, helpers_1.handleError)(err, res);
});
exports.default = app;
