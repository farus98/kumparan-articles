"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function buildMakeArticles(moment) {
    return function makeArticles({ author = '', title = '', body = '', createdTime = moment().format('YYYY-MM-DD HH:mm:ss'), updateTime = moment().format('YYYY-MM-DD HH:mm:ss') } = {}) {
        if (!author) {
            throw new Error('author must be exist');
        }
        if (!title) {
            throw new Error('title must be exist');
        }
        if (!body) {
            throw new Error('body must be exist');
        }
        return Object.freeze({
            getAuthor: () => author,
            getTitle: () => title,
            getBody: () => body,
            getCreatedTime: () => createdTime,
            getUpdatedTime: () => updateTime
        });
    };
}
exports.default = buildMakeArticles;
