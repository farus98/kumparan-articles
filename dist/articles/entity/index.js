"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const articles_1 = __importDefault(require("./articles"));
const moment_1 = __importDefault(require("moment"));
moment_1.default.locale('id');
const makeArticles = (0, articles_1.default)(moment_1.default);
exports.default = makeArticles;
