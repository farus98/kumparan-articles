"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ioredis_1 = __importDefault(require("ioredis"));
class makeRedis {
    connect() {
        return __awaiter(this, void 0, void 0, function* () {
            let redis;
            let port = process.env.REDIS_PORT;
            let host = process.env.REDIS_HOST;
            let pass = process.env.REDIS_PASSWORD;
            const redisConfig = {
                port: port,
                host: host,
                password: pass,
                autoResubscribe: false,
                lazyConnect: true,
                maxRetriesPerRequest: 0, // <-- this seems to prevent retries and allow for try/catch
            };
            try {
                redis = new ioredis_1.default(redisConfig);
                const infoString = yield redis.info();
                // console.log(infoString)
                return redis;
            }
            catch (err) {
                console.log("Redis connection fail");
                yield redis.disconnect();
                // do nothing
                return false;
            }
        });
    }
}
exports.default = makeRedis;
