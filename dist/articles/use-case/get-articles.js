"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_timezone_1 = __importDefault(require("moment-timezone"));
moment_timezone_1.default.locale('id');
const redis_1 = __importDefault(require("../helpers/redis"));
function makeGetArticles({ articlesDb }) {
    return function getArticles(body) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const redis = new redis_1.default();
                const redisClient = yield redis.connect();
                let result;
                if (redisClient != false) {
                    if (body.author || body.query) {
                        let key;
                        if (body.author && body.query) {
                            key = "author = " + body.author + " and query = " + body.query;
                        }
                        else if (body.author) {
                            key = "author = " + body.author;
                        }
                        else if (body.query) {
                            key = "query = " + body.query;
                        }
                        else {
                            key = "all";
                        }
                        let getCacheData = yield redisClient.get(key);
                        if (getCacheData == null) {
                            const articles = yield getArticlesDb(body);
                            if (articles.totalData > 0) {
                                yield redisClient.set(key, JSON.stringify(articles.data));
                            }
                            result = articles;
                        }
                        else {
                            const data = JSON.parse(getCacheData);
                            result = { responseCode: 200, data: data, totalData: data.length, info: 'data from cache' };
                        }
                    }
                    else {
                        let getCacheData = yield redisClient.get("all");
                        const data = JSON.parse(getCacheData);
                        result = { responseCode: 200, data: data, totalData: data.length, info: 'data from cache all' };
                    }
                }
                else {
                    result = yield getArticlesDb(body);
                }
                return result;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    };
    function getArticlesDb(body) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result;
                const getData = yield articlesDb.showDataArticles(body);
                if (getData.length > 0) {
                    const articlesData = yield Promise.all(getData.map(data => {
                        const created = (0, moment_timezone_1.default)(data.created).tz("Asia/Jakarta").format('YYYY-MM-DD HH:mm:ss');
                        delete data.created;
                        data.created = created;
                        return data;
                    }));
                    result = { responseCode: 200, data: articlesData, totalData: getData.length, info: 'data from db' };
                }
                else {
                    result = { responseCode: 204, data: [], totalData: 0 };
                }
                return result;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
}
exports.default = makeGetArticles;
