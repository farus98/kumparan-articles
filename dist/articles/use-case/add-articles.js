"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const redis_1 = __importDefault(require("../helpers/redis"));
const moment_timezone_1 = __importDefault(require("moment-timezone"));
moment_timezone_1.default.locale('id');
function makeAddArticles({ articlesDb, makeArticles }) {
    return function addArticles(body) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result;
                const entityArticles = yield makeArticles(body);
                const insertData = yield articlesDb.createDataArticles(entityArticles);
                if (insertData > 0) {
                    const redis = new redis_1.default();
                    const redisClient = yield redis.connect();
                    if (redisClient != false) {
                        const getData = yield articlesDb.showDataArticles({ null: null });
                        const articlesData = yield Promise.all(getData.map(data => {
                            const created = (0, moment_timezone_1.default)(data.created).tz("Asia/Jakarta").format('YYYY-MM-DD HH:mm:ss');
                            delete data.created;
                            data.created = created;
                            return data;
                        }));
                        redisClient.set("all", JSON.stringify(articlesData));
                    }
                    result = { responseCode: 201, information: "Article saved successfully" };
                }
                else {
                    result = { responseCode: 400, information: "Article failed to save" };
                }
                return result;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    };
}
exports.default = makeAddArticles;
