"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDataArticles = exports.createDataArticles = void 0;
const data_access_1 = require("../data-access");
const entity_1 = __importDefault(require("../entity"));
const add_articles_1 = __importDefault(require("./add-articles"));
const get_articles_1 = __importDefault(require("./get-articles"));
const createDataArticles = (0, add_articles_1.default)({ articlesDb: data_access_1.articlesDb, makeArticles: entity_1.default });
exports.createDataArticles = createDataArticles;
const getDataArticles = (0, get_articles_1.default)({ articlesDb: data_access_1.articlesDb });
exports.getDataArticles = getDataArticles;
const kumparanService = Object.freeze({
    createDataArticles,
    getDataArticles
});
exports.default = kumparanService;
