const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../dist/articles/index')

chai.use(chaiHttp);
chai.should();

describe("Test Article Endpoint", () => {
  it("Get All Articles", (done) => {
    chai.request(app.default)
      .get('/articles')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        done();
      });
  });

  it("Get Articles With Query Parameters", (done) => {
    chai.request(app.default)
      .get('/articles?author=diana&query=dil')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        done();
      });
  });

  it("Add Article (POST)", (done) => {
    chai.request(app.default)
      .post('/articles')
      .set('X-API-Key', 'foobar')
      .send({ author:"fildzah", title:"kesmas 1", body:"isi kesmas 1"})
      .end((err, res) => {
        res.should.have.status(201);
        res.body.should.be.a('object');
        done();
      });
  });

});
