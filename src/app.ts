import express from 'express';
import articles from './articles'

const app = express();

app.use('/kumparan', articles);

app.listen(3000,()=> console.log('server run in port 3000'));
