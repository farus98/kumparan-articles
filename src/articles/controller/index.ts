import {
    createDataArticles, 
    getDataArticles
} from "../use-case"

import makeAddArticles from "./add-articles"
import makeGetArticles from "./get-articles"

const addArticles = makeAddArticles({createDataArticles})
const getArticles = makeGetArticles({getDataArticles})

const kumparanController = Object.freeze({
    addArticles, 
    getArticles    
})

export default kumparanController
export {
    addArticles, 
    getArticles 
}