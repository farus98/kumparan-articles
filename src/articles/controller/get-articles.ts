export default function makeGetArticles({getDataArticles}) {
  return async function getArticles(httpRequest) {
    try {
      const bodyparam  = httpRequest.query

      const posted = await getDataArticles(bodyparam)
    
      return {
        headers: {
          'Content-Type': 'application/json',
        },
        statusCode: 200,
        body: {
          status : true,
          ...posted
        }
      }
    } catch (err:any) {

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 400,
        body: {
          status : false,
          response_code : 400,
          message: err.message
        }
      }
    }
  }           
} 