export default function makeAddArticles({createDataArticles}) {
  return async function addArticles(httpRequest) {
    try {
      const bodyparam  = httpRequest.body

      const posted = await createDataArticles(bodyparam)

      return {
        headers: {
          'Content-Type': 'application/json',
        },
        statusCode: 201,
        body: {
          status : true,
          ...posted
        }
      }
    } catch (err:any) {

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 400,
        body: {
          status : false,
          response_code : 400,
          message: err.message
        }
      }
    }
  }       
} 