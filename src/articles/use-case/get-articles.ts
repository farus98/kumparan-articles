import moment from 'moment-timezone'
moment.locale('id')

import makeRedis from '../helpers/redis'

export default function makeGetArticles({articlesDb}) {
  return async function getArticles(body:any) {
    try {
      const redis = new makeRedis();
      const redisClient = await redis.connect()
      
      let result

      if(redisClient != false){
        if(body.author || body.query){
        
          let key
          if(body.author && body.query){
            key = "author = "+body.author+" and query = "+body.query
          }else if (body.author){
            key = "author = "+body.author
          }else if(body.query){
            key = "query = "+body.query
          }else{
            key = "all"
          }

          let getCacheData = await redisClient.get(key)

          if(getCacheData == null){
            const articles =  await getArticlesDb(body)

            if(articles.totalData > 0){
              await redisClient.set(key, JSON.stringify(articles.data))
            }

            result = articles
          
          }else{
            const data = JSON.parse(getCacheData)
            result = {responseCode: 200, data: data, totalData: data.length, info: 'data from cache'}
          }
            
        }else{
          let getCacheData = await redisClient.get("all")
          
          const data = JSON.parse(getCacheData)
          result = {responseCode: 200, data: data, totalData: data.length, info: 'data from cache all'}
        }
      }else{
        result = await getArticlesDb(body)
      }
     
      
      

      return result

    } catch (error:any) {
      throw new Error(error);
    }
  }  
  
  async function getArticlesDb(body:any) {
    try {
      
      let result 
      const getData =  await articlesDb.showDataArticles(body)
      if(getData.length > 0){
        const articlesData = await Promise.all(
          getData.map(data=>{
            const created = moment(data.created).tz("Asia/Jakarta").format('YYYY-MM-DD HH:mm:ss')
            delete data.created
            data.created = created

            return data
          })
        ) 
        
        result = {responseCode: 200, data: articlesData, totalData: getData.length, info: 'data from db'}
      }else{
        result = {responseCode: 204, data: [], totalData: 0}
      }

      return result

    } catch (error:any) {
      throw new Error(error);
    }
  }  
  
}  