import makeRedis from '../helpers/redis'
import moment from 'moment-timezone'
moment.locale('id')

export default function makeAddArticles({articlesDb, makeArticles}) {
  return async function addArticles(body:any) {
    try {
    
      let result

      const entityArticles = await makeArticles(body)
      const insertData = await articlesDb.createDataArticles(entityArticles)

      if(insertData > 0){      
        const redis = new makeRedis();
        const redisClient = await redis.connect()
        if(redisClient != false){
          const getData =  await articlesDb.showDataArticles({null: null})
          const articlesData = await Promise.all(
            getData.map(data=>{
              const created = moment(data.created).tz("Asia/Jakarta").format('YYYY-MM-DD HH:mm:ss')
              delete data.created
              data.created = created

              return data
            })
          ) 
        
          redisClient.set("all", JSON.stringify(articlesData))
        }

        result = {responseCode: 201, information: "Article saved successfully"}
      }else{
        result = {responseCode: 400, information: "Article failed to save"}
      }

      return result
  
    } catch (error:any) {
      throw new Error(error);
    }
  
  }        
}  