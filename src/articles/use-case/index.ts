import { articlesDb } from "../data-access"
import makeArticles from "../entity"

import makeAddArticles from "./add-articles"
import makeGetArticles from "./get-articles"

const createDataArticles = makeAddArticles({articlesDb,makeArticles})
const getDataArticles = makeGetArticles({articlesDb})

const kumparanService = Object.freeze({
    createDataArticles, 
    getDataArticles
})

export default kumparanService
export {
    createDataArticles, 
    getDataArticles
}