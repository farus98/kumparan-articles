import Redis from 'ioredis';

export default class makeRedis{
    public async connect(){
        let redis;
        let port:any = process.env.REDIS_PORT
        let host:any = process.env.REDIS_HOST
        let pass:any = process.env.REDIS_PASSWORD

        
        const redisConfig: Redis.RedisOptions = {
            port: port,
            host: host,
            password: pass,
            autoResubscribe: false,
            lazyConnect: true,
            maxRetriesPerRequest: 0, // <-- this seems to prevent retries and allow for try/catch
        };
​
        try {
​
            redis = new Redis(redisConfig);
            const infoString = await redis.info();
            // console.log(infoString)
            return redis
​
        } catch (err) {
​
            console.log("Redis connection fail");
            await redis.disconnect();
            // do nothing
            return false
        }    
    }
}
