import buildMakeArticles from './articles'
import moment from 'moment'

moment.locale('id')
const makeArticles = buildMakeArticles(moment)

export default makeArticles
