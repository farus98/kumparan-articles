export default function makeArticlesDb({Query}) {
    return Object.freeze({
        createDataArticles,
        showDataArticles
    })

    async function createDataArticles(body){
      return new Promise(async function(resolve, reject) {
        try{
          
          let sql = `INSERT INTO articles (author, title, body, created)
                      VALUES ('${body.getAuthor()}', '${body.getTitle()}', '${body.getBody()}', '${body.getCreatedTime()}');`;
          
          let result = await Query(sql);
  
          resolve(result.rowCount)
        
        } catch(error){
          reject(new Error('articlesDb-createDataArticles '+error));
        }
      })
    }


    async function showDataArticles(body){
        return new Promise(async function(resolve, reject) {
          try{
            let where = 'WHERE 1=1 '

            if(body.query){
              where += " AND (title LIKE '%"+body.query+"%' OR body LIKE'%"+body.query+"%') ";
            }
            
            if(body.author){
              where += " AND author = '"+body.author+"' ";
            }

            const  sql = `SELECT * FROM articles ${where} ORDER BY created desc`;   
            console.log('sql',sql);
            
            let result = await Query(sql);
    
            resolve(result.rows)
          
          } catch(error){
            reject(new Error('articlesDb-showDataArticles '+error));
          }
        })
      }

}
  