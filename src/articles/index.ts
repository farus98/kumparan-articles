import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import camelCaseKeys from 'camelcase-keys'

import {ErrorHandler, handleError} from './helpers'
const app = express();

const makeCallback = require ('./call-back')

import {
addArticles,
getArticles
} from "./controller"

app.use(cors());
app.use(bodyParser.json());

app.post('/articles', makeCallback(addArticles,camelCaseKeys))
app.get('/articles', makeCallback(getArticles,camelCaseKeys));

app.use((err:any, req:any, res:any, next:any) => {
    handleError(err, res);
});

export default app;
